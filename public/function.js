/**
 * sorts the table
 * 
 * @param {HTMLTableElement} table to be sorted
 * @param {number} column column to sort on
 * @param {boolean} asc sorting ascending or descending
 */

function sortTableByColumn(table, column, asc = true){
    const dirModifier = asc ? 1 : -1;
    const tBody = table.tBodies[0];
    const rows = Array.from(tBody.querySelectorAll("tr"));

    // sort rows
    const sortedRows = rows.sort((a, b)=>{
  
      // td:nth-child <- sodu selector, get the table-cell data at column + 1
      const aColText = a.querySelector(`td:nth-child(${column+1})`).textContent.trim();
      const bColText = b.querySelector(`td:nth-child(${column+1})`).textContent.trim();

      return aColText > bColText ? (1 * dirModifier) : (-1 * dirModifier);
    })

    // remove rows
    while(tBody.firstChild){
      tBody.removeChild(tBody.firstChild);
    }

    // add sorted rows
    tBody.append(...sortedRows);

    // remove sorting from table
    table.querySelectorAll("th").forEach((th) =>{
      th.classList.remove("th-sort-asc", "th-sort-desc");
    })

    // set new sorting on table
    table.querySelector(`th:nth-child(${ column + 1})`).classList.toggle("th-sort-asc", asc)
    table.querySelector(`th:nth-child(${ column + 1})`).classList.toggle("th-sort-desc", !asc)
}



document.querySelectorAll(".table-sortable th").forEach((headerCell)=>{

  headerCell.addEventListener("click", ()=>{
    // move up from th => tr => table
    const tableElement = headerCell.parentElement.parentElement.parentElement;
    const headerIndex = Array.prototype.indexOf.call(headerCell.parentElement.children, headerCell);
    const currentIsAscending = headerCell.classList.contains('th-sort-asc');

    sortTableByColumn(tableElement, headerIndex, !currentIsAscending)
  })

})

document.addEventListener("DOMContentLoaded", function() {
  nextTable(3)
});


function nextTable(startWithFriend){

  var table = document.getElementById("myTbody")
  const friendList= [
    {
    image: "./Assets/friend1.png",
    name: "James Holden",
    desc: "Wu-tang"
    },
    {
    image: "./Assets/friend2.png",
    name: "Alex Wood",
    desc: "Clan"
    },
    {
    image: "./Assets/friend3.png",
    name: "Xiamoe me",
    desc: "Aint"
    },
    {
    image: "./Assets/friend4.png",
    name: "Aretha Hill",
    desc: "Nothin"
    },
    {
    image: "./Assets/friend5.png",
    name: "Edward leopard",
    desc: "To"
    },
    {
    image: "./Assets/friend6.png",
    name: "Dodge mc fidly",
    desc: "F*"
    },
    {
    image: "./Assets/friend7.png",
    name: "Holden James",
    desc: "With"
    },
    {
    image: "./Assets/friend8.png",
    name: "Jolden Hames",
    desc: "..."
    },
    {
    image: "./Assets/friend4.png",
    name: "Aretha Hill",
    desc: "Nothin"
    },
    {
    image: "./Assets/friend5.png",
    name: "Edward leopard",
    desc: "To"
    }

  ]
  
  
  while(table.firstChild){
    table.removeChild(table.firstChild);
  }

  for (let i = startWithFriend; i < startWithFriend+5; i++) {
    const element = friendList[i];

    var row = table.insertRow(0);
    var cell1 = row.insertCell(0)
    var cell2 = row.insertCell(1)
    var cell3 = row.insertCell(2)
  
    cell1.innerHTML = `<img src="${element.image}" alt="dog friend">`;
    cell2.innerHTML = element.name;
    cell3.innerHTML = element.desc;
    
  }

}